import { LoadingOutlined } from "@ant-design/icons";

const AuthForm = ({
  handleSubmit,
  name,
  setName,
  email,
  setEmail,
  password,
  setPassword,
  secret,
  setSecret,
  username,
  setUsername,
  about,
  setAbout,
  loading,
  setLoading,
  page,
  profileUpdate,
}) => (
  <form onSubmit={handleSubmit}>
    {profileUpdate && (<div className="form-group p-2">
      <small>
        <label className="text-muted">Username</label>
      </small>
      <input
        type="text"
        placeholder="Enter Username"
        className="form-control"
        value={username}
        onChange={(e) => setUsername(e.target.value)}
      />
    </div>)}
    {profileUpdate && (<div className="form-group p-2">
        <small>
            <label className="text-muted">About</label>
        </small>
        <input 
            type="text" 
            placeholder="Tell about yourself"
            className="form-control"
            value={about}
            onChange={e => setAbout(e.target.value)}
        />
    </div>)}
    {page !== "login" && (
      <div className="form-group p-2">
        <small>
          <label className="text-muted">Your Name</label>
        </small>
        <input
          type="text"
          placeholder="Enter Name"
          className="form-control"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
      </div>
    )}
    <div className="form-group p-2">
      <small>
        <label className="text-muted">Email Address</label>
      </small>
      <input
        type="email"
        placeholder="Enter Email Address"
        className="form-control"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        disabled = {profileUpdate}
      />
    </div>
    <div className="form-group p-2">
      <small>
        <label className="text-muted">Password</label>
      </small>
      <input
        type="password"
        placeholder="Enter Password"
        className="form-control"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
    </div>
    {page !== "login" && (
      <>
        <div className="form-group p-2">
          <small>
            <label className="text-muted">Pick a question</label>
          </small>
          <select className="form-control">
            <option>What's your favourite colour?</option>
            <option>What's your best friend's name?</option>
            <option>What city you were born?</option>
          </select>
          <small className="text-muted form-text">
            You can use this to reset your password if forgotten
          </small>
        </div>
        <div className="form-group p-2">
          <input
            type="text"
            className="form-control"
            placeholder="Write your answer here"
            onChange={(e) => setSecret(e.target.value)}
            value={secret}
          />
        </div>
      </>
    )}
    <div className="form-group p-2">
      <button
        disabled={
          profileUpdate 
          ? loading
          : page === "login"
            ? !email || !password
            : !name || !password || !secret || !email
        }
        className="btn btn-primary col-12"
      >
        {loading ? <LoadingOutlined spin className="py-1" /> : "Submit"}
      </button>
    </div>
  </form>
);

export default AuthForm;
