import { useContext } from "react";
import Avatar from "antd/lib/avatar/avatar";
import moment from "moment";
import renderHTML from "react-render-html";
import PostImage from "../images/postImage";
import {
  HeartOutlined,
  HeartFilled,
  CommentOutlined,
  EditOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import { UserContext } from "../../context";
import { useRouter } from "next/router";
import Link from "next/link";
import { imageSource } from "../../functions/index";

const Post = ({
  post,
  handleDelete,
  handleLike,
  handleUnlike,
  handleComment,
  removeComment,
  commentsCount = 2,
  
}) => {
  const [state] = useContext(UserContext);
  const router = useRouter();

  return (
    <>
      {
        post && post.postedBy && (
          <div key={post._id} className="card mb-5">
        <div className="card-header">
          <Avatar size={40} src={imageSource(post.postedBy)} />
          <span className="p-2 ml-3">{post.postedBy.name}</span>
          <span className="p-2 ml-3" style={{ marginLeft: "1rem" }}>
            {moment(post.createdAt).fromNow()}
          </span>
        </div>
        <div className="card-body">{renderHTML(post.content)}</div>
        <div className="card-footer">
          {post.image && <PostImage url={post.image.url} />}
          <div className="d-flex pt-2">
            {state &&
            state.user &&
            post.likes &&
            post.likes.includes(state.user._id) ? (
              <HeartFilled
                className="text-danger pt-2  h5"
                onClick={() => handleUnlike(post._id)}
              />
            ) : (
              <HeartOutlined
                className="text-danger pt-2  h5"
                onClick={() => handleLike(post._id)}
              />
            )}
            <div className="pt-2 px-1" style={{ marginRight: "1rem" }}>
              {post.likes.length > 0 && post.likes.length} Likes
            </div>
            <CommentOutlined
              className="text-danger pt-2  h5 px-1"
              onClick={() => handleComment(post)}
            />
            <div className="pt-2 px-1">
              <Link href={`/post/${post._id}`}>
                <a>
                  {post.comments.length > 0 && post.comments.length} Comments
                </a>
              </Link>
            </div>
            {/* {console.log( state.user , state.user._id , post.postedBy._id )} */}
            {state && state.user && state.user._id === post.postedBy._id && (
              <>
                <EditOutlined
                  className="text-danger pt-2  h5 px-2 mx-auto"
                  onClick={() => router.push(`user/post/${post._id}`)}
                />
                <DeleteOutlined
                  className="text-danger pt-2  h5 px-2"
                  onClick={() => handleDelete(post)}
                />
              </>
            )}
          </div>
        </div>
        {/* 2 comments */}
        {post.comments && post.comments.length > 0 && (
          <ol className="list-group" style={{maxHeight: '125px', overflow:'scroll'}}>
            {post.comments.slice(0, commentsCount).map((c) => (
              <li className="list-group-item d-flex justify-content-between align-items-start" key={c._id}>
                <div className="ms-2 me-auto">
                  <div>
                    <Avatar
                      size={20}
                      className="mb-1 mr-3"
                      src={imageSource(c.postedBy)}
                    />
                    <span className="px-1">{c.postedBy.name}</span>
                  </div>
                  <div>{c.text}</div>
                </div>
                <span className="badge rounded-pill text-muted">
                  {moment(c.created).fromNow()}
                  {state && state.user && state.user._id === c.postedBy._id && (
                    <div className="ml-auto mt-1">
                      <DeleteOutlined className="pl-2 text-danger" 
                        onClick={() => removeComment(post._id, c)} />
                    </div>
                  )}
                </span>
              </li>
            ))}
          </ol>
        )}
      </div>
        )
      }
    </>
  );
};

export default Post;
