import {useState, useContext, useEffect} from 'react';
import {useRouter} from 'next/router';
import UserRoute from '../components/routes/UserRoute';
import PostForm from '../components/forms/PostForm';
import { UserContext } from '../context';
import axios from 'axios';
import { toast } from 'react-toastify';
import PostList from '../components/cards/PostList';
import People from '../components/cards/Peple';
import Link from 'next/link';
import { Modal, Pagination } from 'antd';
import CommentForm from '../components/forms/CommentForm';
import Search from '../components/Search';

const DashBoard = () => {

    const [state, setState] = useContext(UserContext);
    //state
    const [content, setContent] = useState('');
    const [image, setImage] = useState({});
    const [uploading, setUploading] = useState(false);
    //posts
    const [posts, setPosts] = useState([]);
    //people
    const [people, setPeople] = useState([]);
    //comment
    const [comment, setComment] = useState('');
    const [visible, setVisible] = useState(false);
    const [currentPost, setCurrentPost] = useState({});
    //pagination
    const [totalPosts, setTotalPosts] = useState(0);
    const [page, setPage] = useState(1);
    //router
    const router = useRouter();

    useEffect (() => {
        if(state && state.token) {
            newsFeed();
            findPeople();
        }
    }, [state && state.token, page]);

    useEffect(()=> {
        try {
            axios.get('/total-posts').then(({data}) => setTotalPosts(data));
        } catch (err) {
            console.log("err");
        }
    })

    const findPeople = async () => {
        try {
            const { data } = await axios.get('/find-people');
            setPeople(data);
        } catch (err) {
            console.log(err);
        }
    }

    const newsFeed = async () => {
        try {
            const {data} = await axios.get(`/news-feed/${page}`);
            
            setPosts(data);
            // console.log("user posts", data);
        }catch (err) {
            console.log(err);
        }
    }

    const postSubmit =async (e) => {
        e.preventDefault();
        try{
           const {data} = await axios.post('/create-post', {content, image});
           console.log(content, image)
           if(data.error) {
               toast.error(data.error)
           }else {
               setPage(1);
                newsFeed();
                toast.success('Post create....');
                setContent("");
                setImage({});
           }
        //    console.log("p", data)
        }catch (error) {
            console.log(error)
        }
    }

    const handleImage = async (e) => {
        const file = e.target.files[0];
        let formData = new FormData();
        formData.append('image', file);
        console.log([...formData]);
        setUploading(true);
        try {
            const {data} = await axios.post('/upload-image', formData);
            setImage({
                url: data.url,
                public_id: data.public_id
            })
            console.log(data)
        }catch(err) {
            console.log(err)
        }
    }

    const handleDelete = async (post) => {
        try {
            const answer =  window.confirm('Are you sure?');
            if(!answer) return;
            const { data } = await axios.delete(`/delete-post/${post._id}`);
            toast.error('Post deleted!!!');
            newsFeed();
        } catch (err) {
            console.log(err)
        }
    }

    const handleFollow = async  (user) => {
        // console.log("add this user to the following list", user._id);
        try {
            const { data } = await axios.put('/user-follow', {_id : user._id});
            // console.log("data", data);
            let auth = JSON.parse(localStorage.getItem('auth'));
            auth.user = data;
            localStorage.setItem('auth', JSON.stringify(auth));
            setState({...state, user : data});
            let filtered =  people.filter(p => p._id !== user._id);
            setPeople(filtered);
            // render the post in newsfeed
            newsFeed();
            toast.success(`Following ${user.name}...`);
        } catch (err) {
            console.log("err", err)
        }
    }

    const handleLike = async (_id) => {
        console.log("like", _id);
        try {
            const {data} = await axios.put('/like-post' , {_id});
            // console.log("ld", data);
            newsFeed();
        } catch (err) {
            console.log("err")
        }       
    }

    const handleUnlike = async (_id) => {
        try {
            const {data} =await axios.put('/unlike-post' , {_id});
            // console.log("uld", data);
            newsFeed();
        } catch (err) {
            console.log("err")
        }     
    }

    const handleComment = (post) => {
        setCurrentPost(post);
        setVisible(true);
    }

    const addComment = async (e) => {
        e.preventDefault();
        try {
            const { data } = await axios.put('/add-comment', {
                postId : currentPost._id,
                comment
            });
            console.log("crrnt", currentPost._id, "daat", data);
            setComment('');
            setVisible(false);
            newsFeed();
        } catch (err) {
            console.log(err)
        }
    }

    const removeComment = async (postId, comment) => {
        //   console.log(postId, comment);
        let ans = window.confirm("Are you want to delete your comment?");
        if(!ans) return;
    
        try {
            const { data } = await axios.put('/remove-comment', {
                postId,
                comment
            });
            newsFeed();
            console.log("rc", data)
        } catch (err) {
            console.log("err")
        }
      };

    return (
        <UserRoute>
            <div className='container-fluid'>
                <div className="row py-5 bg-default-image text-light">
                    <div className="col text-center">
                        <h1>Newsfeed</h1>
                    </div>
                </div>
                <div className='row py-3'>
                    <div className='col-md-8'>
                        <PostForm
                            content={content}
                            setContent={setContent}
                            postSubmit={postSubmit}
                            handleImage = {handleImage}
                            uploading = {uploading}
                            image = {image}
                        />
                    {/* {JSON.stringify(posts, null, 4)} */}
                        <br/>
                        <PostList 
                            posts={posts} 
                            handleDelete={handleDelete} 
                            handleLike={handleLike} 
                            handleUnlike={handleUnlike}
                            handleComment = {handleComment}
                            removeComment ={removeComment}
                         />
                         
                         <Pagination current={page} total={(totalPosts/3)*10}
                            onChange={(value) => setPage(value)} className='pb-5' />
                    </div>
                        <div className='col-md-4'>
                            <Search />
                            <br/>
                            {state && state.user && state.user.following &&
                                <Link href='/user/following'>
                                    {/* {console.log(state.user)} */}
                                    <a className='h6'>{state.user.following.length} Following</a>
                                </Link>
                            }
                            <People people={people} handleFollow={handleFollow} />
                        </div>
                    
                </div>
            </div>
            <Modal visible = {visible} onCancel={()=> setVisible(false)} title="comment" footer={null}>
                <CommentForm addComment={addComment} comment={comment} setComment={setComment} />
            </Modal>
        </UserRoute>
    )
}

export default DashBoard;