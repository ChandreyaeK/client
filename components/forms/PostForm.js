import {Avatar} from 'antd';
import dynamic from 'next/dynamic';
const ReactQuill = dynamic(()=> import('react-quill'), {ssr:false});
import 'react-quill/dist/quill.snow.css';
import { CameraOutlined, LoadingOutlined } from '@ant-design/icons';
import renderHTML from 'react-render-html';
import moment from 'moment';

const PostForm = ({content, setContent, postSubmit, handleImage, image, uploading}) => {
    return(
        <div className='card'>
            <div className='card-body pb-3'>
                <form className='form-group'>
                    <ReactQuill 
                        theme='snow'
                        className='form-control'
                        placeholder='Write something...'
                        value={content}
                        onChange={e => setContent(e)}
                    />
                </form>
            </div>
            <div className='card-footer d-flex justify-content-between text-muted'>
                <button className='btn btn-primary m-1 btn-sm' onClick={postSubmit} disabled={!content}>Post</button>
                <label>
                    {image && image.url
                        ? (<Avatar />)
                        : uploading 
                            ? <LoadingOutlined className='mt-2'/>
                            : <CameraOutlined className='mt-2' />
                    }
                    <input type="file" accept="images/*" hidden onChange={handleImage} />
                </label>
            </div>
        </div>
    )
}

export default PostForm;