import { useContext } from "react";
import { Avatar, List } from "antd";
import Link from 'next/link';
import { useRouter } from 'next/router';
import moment from "moment";
import { UserContext } from "../../context";

const People = ({people, handleFollow, handleUnFollow}) => {

    const [state] = useContext(UserContext);
    const router = useRouter();

    const imageSource = (user) => {
        if(user.image) {
            return user.image.url;
        } else {
            return '/images/pic.jpg';
        }
    }

    return (
        <>
            {/* {JSON.stringify(people)} */}
            <List itemLayout="horizontal"  dataSource={people} renderItem={(user) => (
                <List.Item>
                    <List.Item.Meta 
                        avatar = { <Avatar src={imageSource(user)} />}
                        title={
                        <div className="d-flex justify-content-between">
                            <Link href={`/user/profile/${user.name}`}>
                                <a>{user.name} </a>
                            </Link>
                            {state && state.user && user.follower && user.follower.includes(state.user._id)
                                ? <span 
                                onClick={()=> handleUnFollow(user)} className="text-primary pointer">
                                Unfollow</span>
                                :
                                <span 
                                onClick={()=> handleFollow(user)} className="text-primary pointer">
                                    Follow</span>
                            }
                        </div>
                    } />
                </List.Item>
            )} />
        </>
    )
}

export default People;