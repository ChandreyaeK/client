import{useContext} from 'react';
import ParallaxBG from '../../../components/cards/ParallaxBG';
import axios from 'axios';
import PostPublic from '../../../components/cards/PostPublic';
import Post from '../../../components/cards/Post';
import Head from 'next/head';
import Link from 'next/link';
import { UserContext } from '../../../context';

const SinglePost = ({post}) => {
    const [state, useState] = useContext(UserContext);
    const head = () => <Head>
        <title>MERNCAMP - Social media by dev for devs</title>
        <meta name="description" content={post.content} />
        <meta property='og:description' content='A Social media for developers' />
        <meta property='og:type' content='Website' />
        <meta property='og:name' content='A Social media for developers' />
        <meta property='og:site_name' content='MERNCAMP' />
        <meta property='og:url' content={`http://merncamp.com/post/view/${post._id}`} />
        <meta property='og:image.secure_url' content={imageSource(post)} />
    </Head>

    const imageSource = (post) => {
        if(post.image) return post.image.url;
        else return '/images/pic.jpg';
    }

    return (
        <>
            {head()}
            <ParallaxBG url="/images/pic.jpg" />
            <div className='container'>
                <div className='row pt-5'>
                    <div  className='col-md-8 offset-md-2'  key={post._id}>
                        {state && state.token ? <Post post={post} />:<PostPublic post={post} />}
                    </div>
                </div>
            </div>
        </>
    )
}

export async function getServerSideProps(context) {
    const {data} = await axios.get(`/post/${context.params._id}`);
    // console.log("home", data);
    return {
      props: {
          post: data
      }, // will be passed to the page component as props
    }
  }

export default SinglePost;