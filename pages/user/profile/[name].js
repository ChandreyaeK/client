import { useContext, useState, useEffect } from "react";
import { Avatar, Card } from "antd";
import { useRouter } from "next/router";
import moment from "moment";
import { UserContext } from "../../../context";
import axios from "axios";
import { RollbackOutlined } from "@ant-design/icons";
import Link from "next/link";
import { imageSource } from "../../../functions/index";

const { Meta } = Card;

const Name = () => {
  const [user, setUser] = useState({});
  const [state, setState] = useContext(UserContext);
  const router = useRouter();

  useEffect(() => {
    if (state && state.token) fetchUser();
  }, [router.query.name]);

  const fetchUser = async () => {
    try {
      const { data } = await axios.get(`/user/${router.query.name}`);
      console.log("qd-->", data);
      setUser(data);
    } catch (err) {}
  };

  const handleUnfollow = async (user) => {
    try {
      const { data } = await axios.put("/user-unfollow", { _id: user._id });
      let auth = JSON.parse(localStorage.getItem("auth"));
      auth.user = data;
      localStorage.setItem("auth", JSON.stringify(auth));
      setState({ ...state, user: data });
      let filtered = people.filter((p) => p._id !== user._id);
      setPeople(filtered);
      // render the post in newsfeed
      newsFeed();
      toast.error(`unfollowed ${user.name}!!!`);
    } catch (err) {}
  };

  return (
    <>
      {/* {JSON.stringify(people)} */}
      <div className="row col-md-6 offset-md-3">
        <div className="pt-5 pb-5">
          <Card hoverable cover={<img src={imageSource(user)} alt={user.name} />}>
              <Meta title={user.name} description={user.about} />
              <p className="pt-5 text-muted">
                  Joined {moment(user.createdAt).fromNow()}
              </p>
              <div className="d-flex justify-content-between">
              <span className="btn btn-sm">
                      {user.follower && user.follower.length} Follower
                  </span>
                  <span className="btn btn-sm">
                      {user.following && user.following.length} Following
                  </span>
              </div>
          </Card>
        </div>
        <Link href="/dashboard">
          <a className="d-flex justify-content-center pt-5">
            <RollbackOutlined />
          </a>
        </Link>
      </div>
    </>
  );
};

export default Name;
