import { LoadingOutlined } from '@ant-design/icons';

const ForgotPasswordForm = ({
    handleSubmit,
    email,
    setEmail,
    newPassword,
    setNewPassword,
    secret,
    setSecret,
    loading,
    setLoading,
    page
}) => (
    <form onSubmit={handleSubmit}>
    <div className="form-group p-2">
        <small>
            <label className="text-muted">Email Address</label>
        </small>
        <input type="email" placeholder="Enter Email Address" 
            className="form-control"
            value={email}
            onChange={e => setEmail(e.target.value)}
        />
    </div>
    <div className="form-group p-2">
        <small>
            <label className="text-muted">New Password</label>
        </small>
        <input type="newPassword" placeholder="Enter New Password" 
            className="form-control"
            value={newPassword}
            onChange={e => setNewPassword(e.target.value)}
        />
    </div>
        <div className="form-group p-2">
            <small>
                <label className="text-muted">Pick a question</label>
            </small>
            <select className="form-control">
                <option>What's your favourite colour?</option>
                <option>What's your best friend's name?</option>
                <option>What city you were born?</option>
            </select>
            <small  className="text-muted form-text">
                You can use this to reset your password if forgotten
            </small>
        </div>
        <div className="form-group p-2">
            <input type="text" className="form-control" 
                placeholder="Write your answer here"
                onChange={e => setSecret(e.target.value)}
                value={secret}
            />
        </div>
    <div className="form-group p-2">
        <button 
            disabled={
            !email || !newPassword || !secret
            }
            className="btn btn-primary col-12">
                {loading ? <LoadingOutlined spin className="py-1" /> :  "Submit"}
        </button>
    </div>
</form>
)

export default ForgotPasswordForm;