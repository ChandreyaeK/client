import { useState, useEffect, createContext } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import { route } from 'next/dist/server/router';

const UserContext = createContext();

const UserProvider = ({children}) => {
    const [state, setState] = useState({
        user: {},
        token : ""
    })

    const router = useRouter();

    const token = state && state.token ? state.token : "";
    axios.defaults.baseURL = process.env.NEXT_PUBLIC_API;
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;

    useEffect(()=> {
        setState(JSON.parse(window.localStorage.getItem('auth')))
        // console.log("state", window.localStorage.getItem('auth'))
    }, []);

    axios.interceptors.response.use(function (response) {
        // Do something before request is sent
        return response;
      }, function (error) {
        // Do something with request error
        let res = err.response;
        if(res.status && res.config && res.config.__isRetryRequest) {
            setState(null);
            window.localStorage.removeItem('auth');
            router.push('/login');
        }
        return Promise.reject(error);
      });

    return <UserContext.Provider value={[state, setState]}>
        {children}
    </UserContext.Provider>
}

export {UserContext, UserProvider};