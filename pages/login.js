import { useState, useContext } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import Link from 'next/link';
import AuthForm from '../components/forms/AuthForm';
import { useRouter } from 'next/router';
import { UserContext } from '../context';

const Login = () => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);
    const [state, setState] = useContext(UserContext);

    const router = useRouter();

    const handleSubmit = async (e) => {
        e.preventDefault();
        // console.log(name, email, password, secret)
       try {
        setLoading(true);
        const {data} = await axios.post('/login', {
            email,
            password,
        })

        if(data.error) {
            toast.error(data.error);
            setLoading(false);
        }else{
            setState({
                user: data.user,
                token: data.token
            })
            window.localStorage.setItem('auth', JSON.stringify(data));
            router.push('/dashboard');
        }
        console.log("-->", data)
       }catch (err) {
        toast.error(err);
        setLoading(false);
       }
    }

    if(state && state.token)  router.push('/dashboard');

    return (
        <div className="container-fluid">
            <div className="row py-5 bg-default-image text-light">
                <div className="col text-center">
                    <h1>Login</h1>
                </div>
            </div>
            {/* {JSON.stringify(state)} */}
            <div className="row py-5">
                <div className="col-md-6 offset-md-3">
                   
                    <AuthForm 
                        handleSubmit={handleSubmit}
                        email = {email}
                        setEmail = {setEmail}
                        password = {password}
                        setPassword = {setPassword}
                        loading = {loading}
                        setLoading = {setLoading}
                        page = "login"
                     />

                </div>
            </div>
            <div className='row'>
                <div className='col'>
                    <p className='text-center'>Not yet registered?
                        <Link href='/register'>
                            <a>Register</a>
                        </Link> 
                    </p>
                </div>
            </div>
            <div className='row'>
                <div className='col'>
                    <p className='text-center'>
                        <Link href='/forgot-password'>
                            <a>Forgot Password?</a>
                        </Link> 
                    </p>
                </div>
            </div>
        </div>
    )
}

export default Login;