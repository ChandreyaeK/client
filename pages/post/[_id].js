import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import axios from "axios";
import Link from 'next/link';
import UserRoute from "../../components/routes/UserRoute";
import { toast } from "react-toastify";
import Post from "../../components/cards/Post";
import { RollbackOutlined } from "@ant-design/icons";

const PostComments = () => {
  const [post, setPost] = useState({});
  const router = useRouter();
  const _id = router.query._id;

  useEffect(() => {
    if (_id) fetchPost();
  }, [_id]);

  const fetchPost = async () => {
    try {
      const { data } = await axios.get(`/user-post/${_id}`);

      setPost(data);
    } catch (err) {
      console.log("err");
    }
  };

  const removeComment = async (postId, comment) => {
    //   console.log(postId, comment);
    let ans = window.confirm("Are you want to delete your comment?");
    if(!ans) return;

    try {
        const { data } = await axios.put('/remove-comment', {
            postId,
            comment
        });
        fetchPost();
        console.log("rc", data)
    } catch (err) {
        console.log("err")
    }
  };
  return (
    <div className="container-fluid">
      <div className="row py-5 bg-default-image text-light">
        <div className="col text-center">
          <h1>MernCamp</h1>
        </div>
      </div>
      <div className="container col-md-8 offset-md-2 pt-5">
        <Post post={post} commentsCount={100} removeComment={removeComment} />
      </div>
      <Link href='/dashboard'>
          <a className="d-flex justify-content-center p-5">
              <RollbackOutlined />
          </a>
      </Link>
    </div>
  );
};

export default PostComments;
