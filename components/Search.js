import axios from "axios";
import { useState, useContext } from "react";
import { UserContext } from "../context";
import People from "./cards/Peple";
import { toast } from "react-toastify";

const Search = () => {
    const [state, setState] = useContext(UserContext);
    const[query, setQuery] = useState("");
    const [result, setResult] = useState([]);

    const searchUser =async (e) => {
        e.preventDefault();
        // console.log(`find ${query} from db`)
        try {
            const {data} = await axios.get(`/search-user/${query}`);
            // console.log("query", data)
            setResult(data);
        } catch (err) {

        }

    }

    const handleFollow = async  (user) => {
        // console.log("add this user to the following list", user._id);
        try {
            const { data } = await axios.put('/user-follow', {_id : user._id});
            // console.log("data", data);
            let auth = JSON.parse(localStorage.getItem('auth'));
            auth.user = data;
            localStorage.setItem('auth', JSON.stringify(auth));
            setState({...state, user : data});
            let filtered =  result.filter(p => p._id !== user._id);
            setResult(filtered);
            toast.success(`Following ${user.name}...`);
        } catch (err) {
            console.log("err", err)
        }
    }

    const handleUnFollow = async (user) => {
        try {
            const { data } = await axios.put('/user-unfollow', {_id: user._id});
            let auth = JSON.parse(localStorage.getItem('auth'));
            auth.user = data;
            localStorage.setItem('auth', JSON.stringify(auth));
            setState({...state, user : data});
            let filtered =  result.filter(p => p._id !== user._id);
            setResult(filtered);
            toast.error(`unfollowed ${user.name}!!!`);
        } catch (err) {

        }
    }

    return <>
        <form className="form-inline row" onSubmit={searchUser}>
           <div className="col-8">
           <input
                className="form-control mr-sm-2 col"
                type="search"
                placeholder="Search"
                value={query}
                onChange={(e)=>{setQuery(e.target.value); setResult([]);}}
             />
           </div>
            <div className="col-4">
            <button className='btn btn-outline-primary col-12' type="submit">
                 Search
             </button>
            </div>
        </form>
        {result && result.map(r=> <People key={r._id} people={result} handleFollow={handleFollow} handleUnFollow={handleUnFollow} />)}
    </>
}

export default Search;