import { useContext } from 'react';
import Avatar from "antd/lib/avatar/avatar";
import moment from "moment";
import renderHTML from "react-render-html";
import PostImage from "../images/postImage";
import { HeartOutlined, HeartFilled, CommentOutlined, EditOutlined, DeleteOutlined } from "@ant-design/icons";
import { UserContext } from '../../context';
import { useRouter } from 'next/router';
import Link from 'next/link';
import {imageSource} from '../../functions/index';
import Post from '../cards/Post';

const PostList = ({posts, handleDelete, handleLike, handleUnlike, handleComment, removeComment}) => {
    const [state] = useContext(UserContext);
    const router = useRouter();

    return (
        <>
            {posts && posts.map(post=> (
                <Post key={post._id}
                    post={post}
                    handleDelete={handleDelete}
                    handleLike={handleLike}
                    handleUnlike = {handleUnlike}
                    handleComment = {handleComment}
                    removeComment={removeComment}
                />
            ))}
        </>
    )
}

export default PostList;