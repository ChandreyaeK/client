import{useContext} from 'react';
import { UserContext } from "../context";
import ParallaxBG from '../components/cards/ParallaxBG';
import axios from 'axios';
import PostPublic from '../components/cards/PostPublic';
import Post from '../components/cards/Post';
import Head from 'next/head';
import Link from 'next/link';

const Home = ({posts}) => {
    const [state, useState] = useContext(UserContext);
    const head = () => <Head>
        <title>MERNCAMP - Social media by dev for devs</title>
        <meta name="description" content="A Social media for developers" />
        <meta property='og:description' content='A Social media for developers' />
        <meta property='og:type' content='Website' />
        <meta property='og:name' content='A Social media for developers' />
        <meta property='og:site_name' content='MERNCAMP' />
        <meta property='og:url' content='http://merncamp.com' />
        <meta property='og:image.secure_url' content='http://merncamp.com/images/pic.jpg' />
    </Head>
    return (
        <>
            {head()}
            <ParallaxBG url="/images/pic.jpg" />
            <div className='container'>
                <div className='row pt-5'>
                {posts && posts.map(post => (
                    <div  className='col-md-4'  key={post._id}>
                    <Link href={`/post/view/${post._id}`}>
                        <a> <PostPublic post={post} /></a>
                    </Link>
                    </div>
                ))}
                </div>
            </div>
        </>
    )
}

export async function getServerSideProps(context) {
    const {data} = await axios.get('/posts');
    return {
      props: {
          posts: data
      }, // will be passed to the page component as props
    }
  }

export default Home;