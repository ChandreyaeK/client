import{useContext} from 'react';

const ParallaxBG = ({url, children="MERNCAMP"}) => {
    // const [state, useState] = useContext(UserContext);
    return (
        <div className="container-fluid" style={{
            backgroundImage: "url(" + url + ")",
            backgroundAttachment: "fixed",
            padding: "100px 0 75px",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            backgroundPosition: "cover cover",
            display: "block",
            objectFit: "contain",
        }}>
           
        
                    {/* {JSON.stringify(state)} */}
                    <h1 className='display-1 font-weight-bold text-center '>{children}</h1>
                
        </div>
    )
}

export default ParallaxBG;