import { useContext, useState, useEffect } from "react";
import { Avatar, List } from "antd";
import { useRouter } from 'next/router';
import moment from "moment";
import { UserContext } from "../../context";
import axios from "axios";
import { RollbackOutlined } from "@ant-design/icons";
import Link from 'next/link';
import { imageSource } from '../../functions/index';

const Following = () => {

    const [people, setPeople] = useState([]);
    const [state, setState] = useContext(UserContext);
    const router = useRouter();

    useEffect(()=>{
        if(state && state.token) fetchFollowing();
    }, [state && state.token]);

    const fetchFollowing = async () => {
        try {
            const { data } = await axios.get('/user-following');
            // console.log("-->",state.user);
            setPeople(data);
        } catch (err) {

        }
    }

    const handleUnfollow = async (user) => {
        try {
            const { data } = await axios.put('/user-unfollow', {_id: user._id});
            let auth = JSON.parse(localStorage.getItem('auth'));
            auth.user = data;
            localStorage.setItem('auth', JSON.stringify(auth));
            setState({...state, user : data});
            let filtered =  people.filter(p => p._id !== user._id);
            setPeople(filtered);
            // render the post in newsfeed
            newsFeed();
            toast.error(`unfollowed ${user.name}!!!`);
        } catch (err) {

        }
    }

    return (
        <>
            {/* {JSON.stringify(people)} */}
            <div className="row col-md-6 offset-md-3">
            <List itemLayout="horizontal"  dataSource={people} renderItem={(user) => (
                <List.Item>
                    <List.Item.Meta 
                        avatar = { <Avatar src={imageSource(user)} />}
                        title={
                        <div className="d-flex justify-content-between">
                            {user.name} <span onClick={()=> handleUnfollow(user)} className="text-primary pointer">Unfollow</span>
                        </div>
                    } />
                </List.Item>
            )} />
            <Link href='/dashboard'>
                <a className="d-flex justify-content-center pt-5">
                    <RollbackOutlined />
                </a>
            </Link>
            </div>
        </>
    )
}

export default Following;